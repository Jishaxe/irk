// Let's require some stuff
// *express* for HTTP server for interfacing with Twilio
var express = require('express')
// *globals* are my global config stuff
var globals = require('./globals')

// *witai* for interacing with wit.ai (natural language processing)
var witai = require('./witai')

// *intents* for handling and running intents
var intents = require('./intents/intents.js')

// *twilio* for SMS interfacing
var twilio = require('twilio')
var twilioApp = twilio(globals.twilio_api_sid, globals.twilio_api_secret)

// Initialize the app
var app = express()

// Respond to root to report we're alive
app.get('/', function(req, res) {
  res.send('Irk is alive!')
})

// The `/sms` endpoint will be triggered when a SMS arrives from Twilio
app.get('/sms', function(req, res) {
  // Twilio sends data to the endpoint as GET params
  var message = req.query.Body

  console.log('I just got a sms: ' + message)
  // First, let's figure out the user's intent with wit.ai
  witai.get_intent(req.query.Body, function(intent) {
    // TODO: Do stuff with the intent

    console.log('I think the user has this intent: ' + JSON.stringify(intent, null, 2))

    // Action the intent and return the output via message
    intents.run_intent(intent, function(output) {
      // Now we'll respond to the user
      var twiml = new twilio.TwimlResponse()

      // Now reply with the output
      twiml.message(output)

      // Now we respond with TwiML, a subset of XML
      res.set('Content-Type', 'text/xml')
      res.send(twiml.toString())
    })
  })
})

// Listen on either $PORT or 3000
app.listen(process.env.PORT || 3000, function() {
  console.log('Irk is alive!')
})
