This is my little cloud-based helper bot, Irk.
=========================================

It responds to SMS commands processed through wit.ai.
This is only a little side project and I'm working on it for experimentation.
I want to extend Irk to automate any daily stuff I have lying around. Not sure what yet.
Some ideas:

- Controlling my LIFX bulb
- Telling my SO where I am
- Notifying me of Jenkins builds
- Notifying me of Twitter stuff
- Telling me when something dramatic happens with the weather
- Controlling my bank account
- Buying stuff from Amazon

Docs are generated with Doccy.
There is a `globals.js` file I haven't included in this repo for privacy reasons.
