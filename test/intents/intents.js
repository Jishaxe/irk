// Test case for intents.js
var intents = require('../../intents/intents.js')
var assert = require('assert')

describe('intents', function() {
  describe('#run_intent', function() {
    it('should report the uptime when ask_uptime intent is passed', function(done) {
      intents.run_intent({'intent': 'ask_uptime'}, function(output) {
        done(assert.notEqual(output.indexOf("I've been up for "), -1))
      })
    })
  })

  describe("#ask_weather", function() {
    it('should report the current weather when ask_weather intent is passed', function(done) {
      intents.run_intent({'intent': 'ask_weather'}, function(output) {
        done(assert.notEqual(output.indexOf("It's "), -1))
      })
    })

    // TODO Finish this test
    it('should fail gracefully when asked for a weather forecast too far or too early in the future', function(done) {
      done()
    })
  })
})
