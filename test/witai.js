var assert = require('assert')
var witai = require('../witai')

// Test case for the witai interface
describe('witai', function() {
  describe('#get_intent', function() {
    it('should return the correct intent when provided with a message', function(done) {
      // The string `Are you there?` should match the intent `ask_uptime`
      witai.get_intent("Are you there?", function(intent) {
        done(assert.equal(intent.intent, 'ask_uptime'))
      })
    })
  })
})
