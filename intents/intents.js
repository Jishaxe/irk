// Gets intents, parses them and hands them off
var ask_weather = require('./ask_weather')

// Recieves a JSON intent from witai and runs it
function run_intent(intent, callback) {
  // Each intent type does a different thing
  try {
    switch(intent.intent) {
      case "ask_uptime":
        callback("I've been up for " + Math.round(process.uptime()) + ' seconds')
        break
      case "ask_weather":
        ask_weather(intent, callback)
        break
      default:
        callback("I'm sorry, I don't know what you're asking for!")
        break
    }
  } catch (err) {
    callback("Something broke! I'm sorry, please give me a moment while I reboot!")
    throw err;
  }
}


module.exports.run_intent = run_intent
