// This module provides interfacing for the Wit.ai API
// HTTP module for making requests to wit.ai
var http = require('https')
var globals = require('./globals')

// The `intent` function recieves a string `message` and returns an intent, as dictated by wit.ai
var get_intent = function(message, callback) {
  // Options to pass to the HTTP library
  var options = {
    // Authorize with OAUTH
    'headers': {'Authorization': 'Bearer ' + globals.witai_api_token},

    // Send the message in GET params
    'host': 'api.wit.ai',
    'path': '/message?v=20160310&q=' + encodeURIComponent(message)
  }

  // Now make the request with those options
  http.request(options, function(response) {
    var str = ''

    response.on('data', function(chunk) {
      str += chunk
    })

    response.on('end', function() {
      // Call the user-specified callback with the complete data in JSON format
      callback(JSON.parse(str).outcomes[0])
    })}).end()
}

module.exports.get_intent = get_intent
