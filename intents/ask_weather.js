// For date processing
var moment = require('moment')
// For interfacing with Forecast.io
var http = require('https')

// For getting the Forecast API key
var globals = require('../globals.js')

// Gets the weather from forecast.io at a certain period
function ask_weather(intent, callback) {
  // Set the time to the time specified by the user. If no time is specified, it's probably now.
  var at_time = null
  try {         at_time = moment(intent.entities.datetime[0].value) }
  catch(err) {  at_time = moment() }

  // Nicely formatted time
  var pretty_time = at_time.format('dddd, MMMM Do YYYY, h:mm:ss a')

  // Currently the coords to Kilmarnock
  var coords = '4.4957,55.6111'



  console.log('Getting the weather at ' + pretty_time)

  // Make a request to Forecast.io in the format
  // `https://api.forecast.io/forecast/APIKEY/LATITUDE,LONGITUDE,TIME`
  var options = {
    host: 'api.forecast.io',
    path: '/forecast/' + globals.forecast_api_key + '/' + coords + ',' + at_time.format('X')
  }

  http.request(options, function(response) {
    var str = ''
    response.on('data', function(data) {
      str += data
    })

    response.on('end', function() {
      var weather_data = JSON.parse(str)

      // If Forecast.IO didn't return with an hourly, it doesn't know the weather at this time
      if (weather_data.hasOwnProperty('hourly')) {
        callback("It's " + JSON.parse(str).hourly.summary.toLowerCase().split('.')[0] + " on " + pretty_time)
      } else {
        callback("I can't see all the way to " + pretty_time + "!")
      }
    })
  }).end()

  //callback("It's partly cloudy at " + pretty_time)
}

module.exports = ask_weather
